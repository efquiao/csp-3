import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AppNavBar from "./components/AppNavBar";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Products from "./pages/Products";
import Specific from "./pages/Specific";
import MyCart from "./pages/MyCart";
import Orders from "./pages/Orders";
import Error from "./pages/Error";
import "./App.css";
import "bootswatch/dist/cosmo/bootstrap.min.css";
import { UserProvider } from "./UserContext";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  useEffect(() => {
    fetch("http://localhost:4000/users/details", {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.id !== "undefined") {
          setUser({ id: data._id, isAdmin: data.isAdmin });
        } else {
          setUser({ id: null, isAdmin: null });
        }
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser }}>
      <Router>
        <AppNavBar />
        <Routes>
          <Route exact path='/' element={<Home />} />
          <Route exact path='/register' element={<Register />} />
          <Route exact path='/login' element={<Login />} />
          <Route exact path='/products' element={<Products />} />
          <Route exact path='/products/:productId' element={<Specific />} />
          <Route exact path='/cart' element={<MyCart />} />
          <Route exact path='/orders' element={<Orders />} />
          <Route exact path='/logout' element={<Logout />} />
          <Route element={Error} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
