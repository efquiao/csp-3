import {useState,useEffect,useContext} from 'react';
import {Card,Button,Row,Col} from 'react-bootstrap';
import {useParams,Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'


const Specific = (props) => {

 
   const {productId} = useParams();

   const {user} = useContext(UserContext);
 

	const [productDetails,setProductDetails] = useState({
 
       name: null,
       description: null,
       image: null,
       price: null
   })
   
   useEffect(()=>{
 
       //fetch to get our course details:
       fetch(`http://localhost:4000/products/${productId}`)
       .then(res => res.json())
       .then(data => {
 
           console.log(data);
           setProductDetails({
 
               name: data.name,
               description: data.description,
               image:data.image,
               price: data.price
 
        })
 
    })
  
},[productId])

 



	return (
 
           <Row className="container">
             
               <Col className="card__column">
                   
                   <Card className="card4">
                       <h1>{productDetails.name}</h1>
                       <Card.Img src={productDetails.image}/>
                       <Card.Body className="text-center">
                        
                           <Card.Subtitle>Description:</Card.Subtitle>
                           <Card.Text>{productDetails.description}</Card.Text>
                           <Card.Subtitle>Price:</Card.Subtitle>
                           <Card.Text>{productDetails.price}</Card.Text>
                       </Card.Body>

                   </Card>
               </Col>
           </Row>
    );
}

export default Specific;