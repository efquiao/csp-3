import React, { useState, useEffect, useContext } from "react";
import AdminView from "../components/AdminView";
import CustomerView from "../components/CustomerView";
import { Container } from "react-bootstrap";

import UserContext from "../UserContext";

export default function Products() {
  const { user } = useContext(UserContext);

  const [products, setProducts] = useState([]);

  const fetchData = () => {
    fetch("http://localhost:4000/products/all")
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  console.log(user.isAdmin)
  return (
    <Container>
      {user.isAdmin === true ? (
        <AdminView productsData={products} fetchData={fetchData} />
      ) : (
        <CustomerView productsData={products} />
      )}
    </Container>
  );
}
