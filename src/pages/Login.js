import React, { useState, useContext } from "react";
import { Form, Button, Card, Row, Col } from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";

const Login = (props) => {
  const history = useNavigate(props);
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [willRedirect, setWillRedirect] = useState(false);

  const authenticate = (e) => {
    e.preventDefault();
    console.log("at login");
    fetch(`http://localhost:4000/users/login`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data.access !== "undefined") {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);
        } else {
          alert(
            "Authentication failed. Please check your login details and try again."
          );
        }
      });
  };

  const retrieveUserDetails = (token) => {
    fetch(`http://localhost:4000/users/details`, {
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({ id: data._id, isAdmin: data.isAdmin });
        console.log(data.isAdmin);
        console.log(props);
        if (data.isAdmin === true) {
          setWillRedirect(true);
        } else {
          if (props === "cart") {
            history.goBack();
          } else {
            setWillRedirect(true);
          }
        }
      });
  };
  console.log(user.isAdmin);
  return willRedirect === true ? (
    user.isAdmin === true ? (
      <Navigate to='/products' />
    ) : (
      <Navigate to='/' />
    )
  ) : (
    <Row className='justify-content-center'>
      <Col xs md='6'>
        <h2 className='text-center my-4'>Log In</h2>
        <Card>
          <Form onSubmit={(e) => authenticate(e)}>
            <Card.Body>
              <Form.Group controlId='userEmail'>
                <Form.Label>Email:</Form.Label>
                <Form.Control
                  type='email'
                  placeholder='Enter your email'
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
              </Form.Group>

              <Form.Group controlId='password'>
                <Form.Label>Password:</Form.Label>
                <Form.Control
                  type='password'
                  placeholder='Enter your password'
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
              </Form.Group>
            </Card.Body>
            <Card.Footer>
              <Button variant='primary' type='submit' block>
                Submit
              </Button>
            </Card.Footer>
          </Form>
        </Card>
        <p className='text-center mt-3'>
          Don't have an account yet? <Link to='/register'>Click here</Link> to
          register.
        </p>
      </Col>
    </Row>
  );
};
export default Login;
